Jira Uploader
================================
A Ruby script for directly uploading attachments to Jira issues

#### Table of Contents
* [Requirements](#markdown-header-requirements)

    - [Required Software](#markdown-header-required-software)

    - [Tested Operating Systems](#markdown-header-tested-operating-systems)

* [Installation](#markdown-header-installation)

    - [Install rest-client](#markdown-header-install-rest-client)

    - [Clone Repository](#markdown-header-clone-repository)

* [Usage](#markdown-header-usage)

    - [Run Methods](#markdown-header-run-methods)
    
    	+ [Passing in Parameters](#markdown-header-passing-in-parameters)
    
    	+ [Using the jira_uploader File](#markdown-header-using-the-jira_uploader-file)

    - [Getting Help](#markdown-header-getting-help)

    - [Running the Script](#markdown-header-running-the-script)

- - -

Requirements
================================
### Required Software
* Ruby
* Ruby Gems
	* rest-client

### Tested Operating Systems
* Fedora 21

Installation
================================
### Install rest-client
Via gem install
```
#!ruby

gem install rest-client
```

### Clone Repository
```
#!shell

git clone http://bitbucket.org/cgigsaocsit/jira-uploader
```

Usage
================================
### Run Methods
You can either pass in the jira_base_path (http://example/jira), username, and password (or be prompted) **OR** you can create the ~/.jira_uploader.yml file that contains this information.

#### Passing in Parameters
```
#!shell

$ ruby jira_uploader.rb -j 'http://localhost:2990/jira' -u admin -f /tmp/test -t DEV-1 --ignore-jira-upload-yml
```

#### Using the jira_uploader File
Create a file under your home directory named .jira_uploader.yml.

EXAMPLE: ~/.jira_uploader.yml

***NOTE***: If you do not include the password line the script will prompt you everytime it is ran for your password. This is much better security wise and is the recommended approach.
```
#!yaml

---
jira_base_path: http://localhost/jira
username: admin
password: admin
```


### Getting Help
```
#!shell

$ ruby jira_uploader.rb  -h
Usage: jira_uploader.rb [options]
    -j                               The base path to the Jira install. https://cm-jira.example.com
    -u, --username USERNAME          Jira username
    -f, --file-path FILEPATH         Path to file to upload
    -t, --jira-ticket JIRATICKET     The ID of the Jira ticket to upload to
    -p, --password PASSWORD          Jira password. If you do not pass this attribute you will be prompted to enter your password after starting the script
    -i, --ignore-jira-upload-yml     Ignore the .jira_uploader.yml in the users home directory
```


### Running the Script
```
#!shell

$ ruby jira_uploader.rb -j 'http://localhost:2990/jira' -u admin -f /tmp/test -t DEV-1 --ignore-jira-upload-yml
Enter Password: 

############################################################
Status Report
************************************************************
Jira Install:			    http://localhost:2990/jira
Jira Username:			    admin
Using ~/jira_uploader.yml:	No
------------------------------------------------------------

Jira Issue Information
------------------------------------------------------------
Jira Issue:		    DEV-1
Jira Attachment:	/tmp/test
############################################################
```
