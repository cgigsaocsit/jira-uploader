require 'rubygems'
require 'rest-client'
require 'optparse'
require 'yaml'
require 'base64'
require 'openssl'

JIRA_UPLOAD_YML = '.jira_uploader.yml'

options = {}
parser = OptionParser.new do |opts|
  opts.banner = "Usage: jira_uploader.rb [options]"

  opts.on('-f', '--file-path FILEPATH', 'Path to file to upload') { |v| options[:file_path] = v }
  opts.on('-t', '--jira-ticket JIRATICKET', 'The ID of the Jira ticket to upload to') { |v| options[:issue] = v }
  opts.on('-j', '--jira-base-url JIRABASEURL', 'The base path to the Jira install. https://cm-jira.example.com') { |v| options[:jira_base_path] = v }
  opts.on('-u', '--username USERNAME', 'Jira username. If you do not pass this attribute you will be prompted to enter your username after starting the script') { |v| options[:username] = v }
  opts.on('-p', '--password PASSWORD', 'Jira password. If you do not pass this attribute you will be prompted to enter your password after starting the script') { |v| options[:password] = v }
  opts.on('-i', '--ignore-jira-upload-yml', "Ignore the #{JIRA_UPLOAD_YML} in the users home directory") { options[:ignore_jira_upload_yml] = true }

end

parser.parse!

class Jira
	END_POINT = '/rest/api/2'
	
	@base_path
	@username
	@password
	
	def initialize(base_path, username, password)
		@base_path 	= base_path
		@username 	= username
		@password 	= password
	end

   	def add_comment(issue, comment)
   		RestClient.post "#{build_base_url}/issue/#{issue}/comment", { :body => comment }.to_json, { :content_type => :json, :accept => :json, :Authorization => build_header_authentication }
   	end

   	def attach_file(issue, file_path)
   		RestClient.post "#{build_base_url}/issue/#{issue}/attachments", { :file => File.new(file_path, 'rb') }, { :accept => :json, :Authorization => build_header_authentication, 'X-Atlassian-Token' => 'no-check' }
   	end

   	private
   	def build_header_authentication
   		"Basic #{ Base64.encode64( "#{@username}:#{@password}" ).chomp}"
   	end

   	def build_base_url
   		"#{@base_path}#{END_POINT}"
   	end
end

def display_report(jira_base_path, username, issue, file_path, used_jira_upload_yml)
	jira_install_string 			= "Jira Install:\t\t\t#{jira_base_path}"
	jira_username_string 			= "Jira Username:\t\t\t#{username}"
	used_jira_upload_yml_string 	= "Using ~/jira_uploader.yml:\t#{used_jira_upload_yml ? 'Yes' : 'No'}"
	jira_issue_string 				= "Jira Issue:\t #{issue}"
	jira_attachment_string 			= "Jira Attachment: #{file_path}"
	
	extra_delim_count = 18

	puts "#" * (jira_install_string.length+extra_delim_count)
	puts 'Status Report'
	puts "*" * (jira_install_string.length+extra_delim_count)
	puts jira_install_string
	puts jira_username_string
	puts used_jira_upload_yml_string
	puts "-" * (jira_install_string.length+extra_delim_count)
	puts ''
	puts 'Jira Issue Information'
	puts "-" * (jira_install_string.length+extra_delim_count)
	puts jira_issue_string
	puts jira_attachment_string
	puts "#" * (jira_install_string.length+extra_delim_count)
end

def get_username
	`read -p "#{'Enter Username'}: " username; echo $username`.chomp	
end

def get_password
	`read -s -p "#{'Enter Password'}: " password; echo $password`.chomp	
end

jira_base_path 	= nil
username 		= nil
password 		= nil
if File.exist?(File.join(File.expand_path('~'), JIRA_UPLOAD_YML)) && !options[:ignore_jira_upload_yml]
	yaml = YAML.load_file(File.join(File.expand_path('~'), JIRA_UPLOAD_YML))
	
	jira_base_path	= yaml['jira_base_path']
	username 		= yaml['username']
	password 		= yaml['password']
	new_lines 		= false
	
	
	unless username
  		username	= get_username
  		new_lines	= true
  	end
  	
	unless password
  		password	= get_password
  		new_lines 	= true
  	end
  	
	
	used_jira_upload_yml	= true
	
	puts "\n\n" if new_lines
else
	jira_base_path 	= options[:jira_base_path]
	new_lines 		= false
	
	unless options[:username]
  		options[:username] 	= get_username
  		new_lines 			= true
  	end
	
	unless options[:password]
  		options[:password] 	= get_password
  		new_lines 			= true
  	end
	
	username 				= options[:username]
	password 				= options[:password]	
	used_jira_upload_yml	= false
	
	puts "\n\n" if new_lines
end

file_path 	= options[:file_path]
issue 		= options[:issue]


unless jira_base_path && username && password
	puts "Error: Missing attribute"
	puts parser.parse '-h' 
	exit(2)
end

unless file_path && File.exist?(file_path)
	puts "Error: Missing or invalid attribute file-path"
	puts parser.parse '-h' 
	exit(3)
end

unless issue
	puts "Error: Missing attribute jira-ticket"
	puts parser.parse '-h' 
	exit(4)
end

client = Jira.new(jira_base_path, username, password)

client.attach_file(issue, file_path)

display_report(jira_base_path, username, issue, file_path, used_jira_upload_yml)
